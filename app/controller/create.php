<?php
namespace app\controller;
use app\model\users;
use app\view;

class create
{
    public function index(){
        return (new view('create'))->render();
    }
    public function save()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_SERVER['REQUEST_URI'] === '/onexpand/index.php/create/save') {
            users::createuser($_POST);
        }
    }
}