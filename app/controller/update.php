<?php

namespace app\controller;
use app\model\users;
use app\view;

class update
{
    public function index($args)
    {
        users::readuser($args);
        return (new view('update', users::readuser($args)))->render();
    }

    public function save()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_SERVER['REQUEST_URI'] === '/onexpand/index.php/update/save') {
           users::edituser($_POST,$_POST['id']);
        }

     }
}