<?php
namespace app\controller;
use app\model\users;
use app\view;

class home
{
public function index(){
    return (new view('index',users::readusers()))->render();
}
public function create()
{
    return (new view('create'))->render();
}
}