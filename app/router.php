<?php
declare(strict_types=1);
namespace app;

class router
{
public  array $routes;
public function register(string $route, array $action):self
{
    $this->routes[$route]=$action;
    return $this;
}
public function resolve(string $requestUR)
{
    $route=explode('index.php',$requestUR);
    $action= $this->routes[$route[count($route)-1]]??null;
    if(is_array($action))
    {
        [$class,$method]=$action;
        if(class_exists($class)){
            $class=new $class();
            if(method_exists($class,$method)){
                return call_user_func_array([$class,$method],[]);
            }
        }
    }
    else{
       $f=explode('?',$route[count($route)-1]);
       if(is_array($f))
       {
           [$class,$method]=$this->routes[$f[0]];

           if(class_exists($class))
           {
               $class=new $class();
               if(method_exists($class,$method)){
                   return call_user_func_array([$class,$method],[$f[1]]);
               }

           }
       }
    }
    if(!$action)
    {
        throw new Exception\Routerexception();
    }
    return call_user_func($action);
}
}