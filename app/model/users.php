<?php
namespace app\model;
class users
{
static function readusers()
{
    return json_decode(file_get_contents("app/model/data/users.json"),associative:true);
}
static function readuser($id)
{
     $data= self::readusers();
         foreach($data as $d)
         {
             if($d['id']==$id)
             {
                 return $d;
             }
        }
}
 static  function GetLastID($users)
    {
        $max=-9999999;
        foreach($users as $user)
        {
            if($user['id']>$max)
            {
                $max=$user['id'];
            }
        }
        return $max+1;
    }

static function createuser($data)
{
    $users=self::readusers();

    $w['id']= self::GetLastID($users);
    $data=$w+$data;
    array_push($users,$data);
    file_put_contents(filename:__DIR__  ."/data/users.json",data:json_encode($users,JSON_PRETTY_PRINT)) ;
    header("Location:http://127.0.0.1/onexpand/index.php/");
}
static function edituser($data,$id)
{
    $users = self::readusers();
    foreach ($users as $key => $val) {
        if ($val['id'] == $id) {
            $users[$key] = $data;
            file_put_contents(filename: __DIR__ . "/data/users.json", data: json_encode($users, JSON_PRETTY_PRINT));
            header("Location:http://127.0.0.1/onexpand/index.php/");
            break;
        }
    }
}
static function deleatuser($id){
    $users = self::readusers();
    foreach ($users as $key => $val) {
        if ($val['id'] == $id) {
            unset($users[$key]);
            file_put_contents(filename: __DIR__ . "/data/users.json", data: json_encode($users, JSON_PRETTY_PRINT));
            header("Location:http://127.0.0.1/onexpand/index.php/");
            break;
        }
    }}
}