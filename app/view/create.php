<?php
include 'partial/header.php';
?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Create New User</h3>
        </div>
        <div class="card-body">

            <form method='POST' action ="create/save">
                <label>Name: </label>
                <input type='text' minlength="10" placeholder="Enter You Name" name='name' required class="form-control" /><br>
                <label>Width: </label>
                <input type='text' minlength="2" pattern="\d+(\.\d{1,2})?" placeholder="Enter Your Width" name='Width' required class="form-control" /><br>

                <label>Length: </label>
                <input type="text" id="Length" pattern="\d+(\.\d{1,2})?" placeholder="Your Length" name="Length" required class="form-control"><br>

                <label>Height: </label>
                <input type="text"  pattern="\d+(\.\d{1,2})?" name="Height" required placeholder="Your Height" class="form-control"/><br>                <input type='submit' value ="Submit" class="btn btn-outline-success"/><br>
            </form>
        </div>
    </div>
</div>
<?php
include 'partial/footer.php';
?>