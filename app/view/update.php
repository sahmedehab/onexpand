<?php
require 'partial/header.php';
?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Update user: <b><?=$this->params['name'] ?></b></h3>
        </div>
        <div class="card-body">
            <form method="post" action="update/save" >
                <?php foreach($this->params as $key=>$val): ?>

                    <div class="form-group">
                        <label><?= ucfirst($key)?>:</label>
                        <?php if($val !=null):?>
                            <input value=<?=$val ?>  name=<?=$key?>   class="form-control"   />
                        <?php endif; ?>
                        <?php if($val ==null):?>
                            <input placeholder="Empty"  name=<?=$key?>   class="form-control"   />
                        <?php endif; ?>
                        <br>
                    </div>
                <?php endforeach; ?>
                <input class="btn btn-success" type="submit" value="Submit">

            </form>
        </div>
    </div>
</div>

<?php  require 'partial/footer.php'; ?>
