<?php
include 'partial/header.php';
?>
<div class="container my-3">
    <H1 class="text-center">PHP CRUD operation using Bootstrap and Ajax</H1>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3  style="display: inline;">All Users</h3>
                <a  href="create" style="display: inline;  float:right"  id="add-user-button" class="btn btn-outline-success">Create New User</a>
            </div>

            <table class="table">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Width</th>
                    <th scope="col">Length</th>
                    <th scope="col">Height</th>
                    <th scope="col">volume</th>
                    <th scope="col">Actions</th>
                </tr>
                <?php foreach($this->params as $user): ?>
                    <tr>
                        <td><?=$user['name'];?></td>
                        <td><?=$user['Width'];?></td>
                        <td><?=$user['Length'];?></td>
                        <td><?=$user['Height'];?></td>
                        <td><?=$user['Height']*$user['Length']*$user['Width'];?></td>
                        <td>
                            <a href="update?<?=$user['id']?>" class="btn btn-outline-secondary">Update</a>
                            <a href="delete?<?=$user['id']?>" class="btn btn-outline-danger">Delete</a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>

</div>
<?php
include 'partial/footer.php';
?>