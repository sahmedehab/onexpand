<?php

namespace app;

class view
{


    /**
     * @param string $string
     */
    public function __construct(string $view,array $params=[])
    {
       $this->view=$view;
       $this->params=$params;
    }

    public function render()
    {
        $loc=VIEW_PATH.'/'.$this->view.'.php';
        include VIEW_PATH.'/'.$this->view.'.php';
    }
}