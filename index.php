<?php
require __DIR__ . '/vendor/autoload.php';
define('VIEW_PATH',__DIR__.'/app/view');
$router=new app\router();
$router->register('/',[app\controller\home::class,'index'])
       ->register('/create',[app\controller\create::class,'index'])
    ->register('/delete',[app\controller\create::class,'index'])
    ->register('/create/save',[app\controller\create::class,'save'])
    ->register('/update',[app\controller\update::class,'index'])
    ->register('/update/save',[app\controller\update::class,'save'])
    ->register('/delete',[app\controller\delete::class,'index']);


echo $router->resolve($_SERVER['REQUEST_URI']);