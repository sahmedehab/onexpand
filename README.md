# User Volume Management System with CRUD Operations and Bootstrap UI 

## Steps to Run the Project
1. Clone the project to your local machine
2. Move the project to the directory `/var/www/html/onexpand`
3. Start the Apache server using the command `sudo /etc/init.d/apache2 start`
4. Visit the URL `127.0.0.1/onexpand/index.php/` in your web browser.

## Project Information
The goal of this project is to create a PHP-based CRUD application that allows users to manage user data through a web interface, with a focus on calculating the volume of each user automatically. The application uses Bootstrap to provide a clean and modern user interface, with a list of users displayed on the main screen.

Each user's details include their name, width, length, height, and calculated volume. The volume is calculated automatically by multiplying the user's height, length, and width, which is displayed alongside the user's details.

The application uses the MVC architecture pattern to separate concerns and ensure code organization. The routes are managed using PHP, and each page is implemented with a corresponding view.

The application's main features include:

1-Create: Users can create new users by filling out a form that collects the user's name, width, length, and height.

2-Read: Users can view a list of all users and their details, including their calculated volume.

3-Update: Users can update the information for any existing user by selecting the user and modifying the relevant fields.

4-Delete: Users can delete any existing user by selecting the user and confirming the deletion.

The application includes basic validation and error handling to ensure data integrity and prevent common errors such as duplicate data or invalid input.

Overall, this project provides a simple and efficient solution for managing user data, with a focus on automatically calculating the volume of each user, making it ideal for use in applications where volume calculation is necessary.
